var express = require('express');
var app = express();
var path = require('path');

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/loginform.html'));
  
});
 app.get('/loginform.js', function(req, res) {
    res.sendFile(path.join(__dirname + '/loginform.js'));
});
app.listen(3000);